  const Footer = () => (
    <div className="footer">
       <p className="mb-0">
        Made with <i className="lni lni-heart-filled"></i>  By :{" "} 
        <a href="https://cyberflow.in" target="_blank">
          Cyber Flow
        </a>
      </p>
    </div>
  )

  export default Footer